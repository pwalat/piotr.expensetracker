﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Piotr.ExpenseTracker.Services.Model;
using Piotr.ExpenseTracker.WindowsApp.Common;
using Piotr.ExpenseTracker.WindowsApp.Services;

namespace Piotr.ExpenseTracker.WindowsApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private bool _isBusy;
        private IEnumerable<Expense> _items;
        private Expense _selectedItem;

        private const string ServiceUrl = "http://localhost:12898/api/expenses";

        public IExpenseService ExpenseService { get; set; }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value != _isBusy)
                {
                    _isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }
        }

        public Expense SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    DeleteCommand.RaiseCanExecuteChanged();
                    UpdateCommand.RaiseCanExecuteChanged();
                    OnPropertyChanged("SelectedItem");
                }
            }
        }

        public IEnumerable<Expense> Items
        {
            get { return _items; }
            set
            {
                if (_items != value)
                {
                    _items = value;
                    OnPropertyChanged("Items");
                }
            }
        }

        #region commands

        public RelayCommand AddCommand { get; set; }
        public RelayCommand RefreshCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }
        public RelayCommand UpdateCommand { get; set; }

        #endregion commands

        public MainViewModel()
        {
            CreateCommands();
            ExpenseService = new JsonNetExpenseService();
        }

        private void CreateCommands()
        {
            AddCommand = new RelayCommand(o => AddHandler());
            RefreshCommand = new RelayCommand(o => RefreshHandler());
            DeleteCommand = new RelayCommand(o => DeleteHandler(),
                                             () => SelectedItem != null);
            UpdateCommand = new RelayCommand(o => UpdateHandler(),
                                             () => SelectedItem != null);
        }

        private void UpdateHandler()
        {
            if(SelectedItem != null)
            {
                var random = new Random();
                var amount = GenerateAmount(random);
                SelectedItem.Amount = amount;
                IsBusy = true;
                ExpenseService.Update(SelectedItem);
                IsBusy = false;
                RefreshData();
            }
        }

        private async void DeleteHandler()
        {
            if(SelectedItem != null)
            {
                IsBusy = true;
                await ExpenseService.Delete(SelectedItem.Id);
                IsBusy = false;
                RefreshData();
            }
        }

        private void RefreshHandler()
        {
            RefreshData();
        }


        private async void AddHandler()
        {
            Random random = new Random();
            int account = random.Next(1, 999);
            var amount = GenerateAmount(random);
            Expense newExpense = new Expense()
                                     {
                                         Account = account.ToString(),
                                         Date = DateTime.UtcNow,
                                         Amount = amount,
                                         Name = GenerateName(random),
                                         Notes = "Some notes",
                                         Type = GenerateType(random),
                                     };
            IsBusy = true;
            await ExpenseService.Add(newExpense);
            IsBusy = false;
            RefreshData();
        }

        private static decimal GenerateAmount(Random random)
        {
            decimal amount = random.Next(10*100, 200*100);
            amount /= 100;
            return amount;
        }

        private string GenerateName(Random random)
        {
            return PickRandom(new[] { "Nerf guns", "Tacos", "Stapler", "Ethernet cable", "Bill", "Invoice", "Meeting with customer" }, random);
        }

        private string GenerateType(Random random)
        {
            return PickRandom(new[] { "Rent", "Lunch", "Training materials", "Office supplies", "Subscriptions", "Computer expenses", "Heat and gas" }, random);
        }

        private string PickRandom(string[] stringPool, Random random)
        {
            var index = random.Next(0, stringPool.Length - 1);
            return stringPool[index];
        }

        public void Load()
        {
            RefreshData();
        }

        private async void RefreshData()
        {
            IsBusy = true;
            Items = await ExpenseService.GetAll();
            IsBusy = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
