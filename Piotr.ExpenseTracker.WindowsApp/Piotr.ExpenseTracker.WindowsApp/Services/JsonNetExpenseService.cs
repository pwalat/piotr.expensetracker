using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Piotr.ExpenseTracker.Services.Model;

namespace Piotr.ExpenseTracker.WindowsApp.Services
{
    public class JsonNetExpenseService : IExpenseService
    {
        private const string ServiceUrl = "http://localhost:12898/api/expenses";
        private readonly HttpClient _client = new HttpClient();

        public async Task<IEnumerable<Expense>> GetAll()
        {
            HttpResponseMessage response = await _client.GetAsync(ServiceUrl);
            var jsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Expense[]>(jsonString);
        }

        public async Task Add(Expense expense)
        {
            var jsonString = Serialize(expense);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var result = await _client.PostAsync(ServiceUrl, content);
        }

        public async Task Delete(Guid id)
        {
            var result = await _client.DeleteAsync(String.Format("{0}/{1}"
                                                                , ServiceUrl, id.ToString()));
        }

        public async Task Update(Expense expense)
        {
            var jsonString = Serialize(expense);
            var content = new StringContent(jsonString,
                                            Encoding.UTF8, "application/json");
            var result = await _client.PutAsync(String.Format("{0}/{1}",
                                                             ServiceUrl, expense.Id), content);
        }

        private string Serialize(Expense expense)
        {
            return JsonConvert.SerializeObject(expense);
        }
    }
}