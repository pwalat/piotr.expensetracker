using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Piotr.ExpenseTracker.Services.Model;

namespace Piotr.ExpenseTracker.WindowsApp.Services
{
    public interface IExpenseService
    {
        Task<IEnumerable<Expense>> GetAll();
        Task Add(Expense expense);
        Task Delete(Guid id);
        Task Update(Expense expense);
    }
}