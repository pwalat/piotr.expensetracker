﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Piotr.ExpenseTracker.Services.Model;

namespace Piotr.ExpenseTracker.WindowsApp.Services
{
    public class ExpenseService : IExpenseService
    {
        private const string ServiceUrl = "http://localhost:12898/api/expenses";
        private readonly HttpClient _client = new HttpClient();

        public async Task<IEnumerable<Expense>> GetAll()
        {
            HttpResponseMessage response = await _client.GetAsync(ServiceUrl);
            var jsonSerializer = CreateDataContractJsonSerializer(typeof(Expense[]));
            var stream = await response.Content.ReadAsStreamAsync();
            return (Expense[])jsonSerializer.ReadObject(stream);
        }

        public async Task Add(Expense expense)
        {

            var jsonString = Serialize(expense);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var result = await _client.PostAsync(ServiceUrl, content);
        }

        public async Task Delete(Guid id)
        {
            var result = await _client.DeleteAsync(String.Format("{0}/{1}"
                 , ServiceUrl, id.ToString()));
        }

        public async Task Update(Expense expense)
        {
            var jsonString = Serialize(expense);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var result = await _client.PutAsync(String
                .Format("{0}/{1}", ServiceUrl, expense.Id), content);
        }

        private static DataContractJsonSerializer CreateDataContractJsonSerializer(Type type)
        {
            const string dateFormat = "yyyy-MM-ddTHH:mm:ss.fffffffZ";
            var settings = new DataContractJsonSerializerSettings
                               {
                                   DateTimeFormat = new DateTimeFormat(dateFormat)
                               };
            var serializer = new DataContractJsonSerializer(type, settings);
            return serializer;
        }

        private string Serialize(Expense expense)
        {
            var jsonSerializer = CreateDataContractJsonSerializer(typeof(Expense));
            byte[] streamArray = null;
            using (var memoryStream = new MemoryStream())
            {
                jsonSerializer.WriteObject(memoryStream, expense);
                streamArray = memoryStream.ToArray();
            }
            string json = Encoding.UTF8.GetString(streamArray, 0, streamArray.Length);
            return json;
        }
    }
}
