using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Piotr.ExpenseTracker.Services.Data;
using Piotr.ExpenseTracker.Services.Model;

namespace Piotr.ExpenseTracker.Services.Controllers
{
    public class ExpensesController : ApiController
    {
        public static IRepository<Expense> ExpenseRepository
            = new InMemoryRepository<Expense>();

        public IEnumerable<Expense> Get()
        {
            var result = ExpenseRepository.Items.ToArray();
            return result;
        }

        public Expense Get(Guid id)
        {
            Expense entity = ExpenseRepository.Get(id);
            if(entity == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return entity;
        }

        public HttpResponseMessage Post(Expense value)
        {
            var result = ExpenseRepository.Add(value);
            if(result == null)
            {
                // the entity with this key already exists
                throw new HttpResponseException(HttpStatusCode.Conflict);
            }
            var response = Request.CreateResponse<Expense>(HttpStatusCode.Created, value);
            string uri = Url.Link("DefaultApi", new { id = value.Id });
            response.Headers.Location = new Uri(uri);  
            return response;
        }

        public HttpResponseMessage Put(Guid id, Expense value)
        {
            value.Id = id;
            var result = ExpenseRepository.Update(value);
            if(result == null)
            {
                // entity does not exist
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public HttpResponseMessage Delete(Guid id)
        {
            var result = ExpenseRepository.Delete(id);
            if(result == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}