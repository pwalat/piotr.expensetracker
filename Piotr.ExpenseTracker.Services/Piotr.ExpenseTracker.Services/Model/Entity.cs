using System;

namespace Piotr.ExpenseTracker.Services.Model
{
    public abstract class Entity
    {
        public Guid Id { get; set; }
    }
}