﻿using System;

namespace Piotr.ExpenseTracker.Services.Model
{
    public class Expense : Entity
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
        public string Notes { get; set; }
        public string Account { get; set; }
    }
}